using Application;
using Infra;
using KafkaFlow;
using KafkaFlow.Serializer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApplicationRegistration();
builder.Services.AddPersistenceRegistration();
builder.Services.ConfigurePersistenceOptions();
builder.Services.AddKafka(kafka =>
    kafka.UseConsoleLog()
    .AddCluster(cluster => {
        cluster.WithBrokers(builder.Configuration.GetSection("KafkaBrokers:Brokers").Get<string[]>() 
            ?? throw new AggregateException("Config:KafkaBrokers:Brokers"));
        cluster.AddProducer("posEvent",
            producer => {
                producer.DefaultTopic("plc-pos-event")
                .AddMiddlewares(d => d.AddSerializer<JsonCoreSerializer>());
            });
    })
);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

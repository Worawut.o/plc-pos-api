﻿using Application.Commands;
using Application.Events;
using Application.Queries;
using Domain.Payloads;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("mailings")]
    [ApiController]
    public class MailingsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public MailingsController(IMediator mediator) 
        {
            _mediator = mediator;
        }

        [HttpPatch]
        [Route("{trackingNo}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SetAsync(
            [FromRoute(Name = "trackingNo")] string trackingNo,
            [FromBody] PatchMailingCommand command)
        {

            if (!ModelState.IsValid) return BadRequest();

            var mail = await _mediator.Send(new GetMailingQuery() { MailingNo = trackingNo });
            if(mail == null) return NotFound();
            
            command.TrackingNo = trackingNo;
            await _mediator.Send(command);

            await _mediator.Publish(new BillClosedEvent() {
                Payload = new BillClosed()
                { 
                    TrackingNo = trackingNo,
                    SysId = mail.SysId,
                    SysName = mail.SysName,
                    DepositaryCode = command.DepositaryCode,
                    DepositaryName = command.DepositaryName,
                    DepositaryIdCard = command.DepositaryIdCard,
                    DepositaryDate = command.DepositaryDate,
                    DepositaryTime = command.DepositaryTime,
                    DepositaryTr = command.DepositaryTr,
                    DepositaryZip = command.DepositaryZip
                }
            });

            return Ok();
        }

    }
}

﻿using Domain.Payloads;
using KafkaFlow;
using RestSharp;
using System.Text.Json;

namespace WebConsumer.Handlers
{
    public class BillCloseHandler : IMessageHandler<BillClosed>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<BillCloseHandler> _logger;
        public BillCloseHandler(
            IServiceProvider serviceProvider,
            ILogger<BillCloseHandler> logger)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(IMessageContext context, BillClosed message)
        {
            _logger.LogWarning(
            "Partition: {0} | Offset: {1}",
            context.ConsumerContext.Partition,
            context.ConsumerContext.Offset);

            var client = new RestClient("https://hook.eu2.make.com/mi6s88m3tyoultozv5u7bdnp1yidrfpd");
            var request = new RestRequest();
            request.Method = Method.Post;
            request.AddHeader("Content-Type", "application/json");
            var body = JsonSerializer.Serialize(message);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            await client.ExecuteAsync(request);
        }
    }
}

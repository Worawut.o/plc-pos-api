using Application;
using Domain.Payloads;
using Infra;
using KafkaFlow;
using KafkaFlow.Admin.Dashboard;
using KafkaFlow.Serializer;
using Microsoft.Extensions.Configuration;
using ProtoBuf.Meta;
using WebConsumer.Handlers;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApplicationRegistration();
builder.Services.AddPersistenceRegistration();
builder.Services.ConfigurePersistenceOptions();

builder.Services.AddKafka(kafka =>
            kafka.UseConsoleLog()
.AddCluster(cluster =>
{
                cluster.WithBrokers(builder.Configuration.GetSection("KafkaBrokers:Brokers").Get<string[]>() 
                    ?? throw new AggregateException("KafkaBrokers:Brokers"));
                cluster.EnableAdminMessages("kafkaflow.admin", "kafkaflow.admin.group.id");
                cluster.EnableTelemetry("kafkaflow.admin", "kafkaflow.telemetry.group.id");
                cluster.CreateTopicIfNotExists("plc-pos-event", 6, 1);
                cluster.AddConsumer(consumer =>
                {
                    consumer.Topic("plc-pos-event");
                    consumer.WithGroupId(builder.Configuration.GetSection("KafkaBrokers:GroupId").Get<string>() 
                        ?? throw new AggregateException("KafkaBrokers:GroupId"));
                    consumer.WithAutoOffsetReset(AutoOffsetReset.Earliest);
                    consumer.WithBufferSize(100);
                    consumer.WithWorkersCount(3);
                    consumer.AddMiddlewares(middlewares => middlewares
                    .AddSingleTypeDeserializer<BillClosed, JsonCoreDeserializer>()
                    .AddTypedHandlers(h => h.AddHandler<BillCloseHandler>()));
                });

            }));

var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.UseKafkaFlowDashboard();

var kafkaBus = app.Services.CreateKafkaBus();
await kafkaBus.StartAsync();

app.Run();

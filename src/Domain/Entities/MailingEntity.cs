﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Entities
{
    public class MailingEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("trackingNo")]
        public string? TrackingNo { get; set; }

        [BsonElement("sysId")]
        public int SysId { get; set; }

        [BsonElement("dataSource")]
        public int DataSource { get; set; } = 1;

        [BsonIgnoreIfNull]
        [BsonElement("sysName")]
        public string? SysName { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryCode")]
        public string? DepositaryCode { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryName")]
        public string? DepositaryName { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryIdCard")]
        public string? DepositaryIdCard { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryDate")]
        public DateTime? DepositaryDate { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryTime")]
        public DateTime? DepositaryTime { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryTr")]
        public string? DepositaryTr { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryZip")]
        public string? DepositaryZip { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("depositaryClient")]
        public string? DepositaryClient { get; set; }
    }
}

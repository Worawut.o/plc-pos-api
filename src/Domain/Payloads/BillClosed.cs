﻿using System.Text.Json.Serialization;

namespace Domain.Payloads
{
    public class BillClosed
    {

        [JsonPropertyName("trackingNo")]
        public string? TrackingNo { get; set; }

        [JsonPropertyName("sysId")]
        public int SysId { get; set; }

        [JsonPropertyName("sysName")]
        public string? SysName { get; set; }

        [JsonPropertyName("depositaryCode")]
        public string? DepositaryCode { get; set; }

        [JsonPropertyName("depositaryName")]
        public string? DepositaryName { get; set; }

        [JsonPropertyName("depositaryIdCard")]
        public string? DepositaryIdCard { get; set; }

        [JsonPropertyName("depositaryDate")]
        public DateTime? DepositaryDate { get; set; }

        [JsonPropertyName("depositaryTime")]
        public DateTime? DepositaryTime { get; set; }

        [JsonPropertyName("depositaryTr")]
        public string? DepositaryTr { get; set; }

        [JsonPropertyName("depositaryZip")]
        public string? DepositaryZip { get; set; }
    }
}

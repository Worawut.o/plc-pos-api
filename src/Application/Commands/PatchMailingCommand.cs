﻿using Application.Interfaces.Repositories;
using Domain.Entities;
using MediatR;
using MongoDB.Driver;
using System.Text.Json.Serialization;

namespace Application.Commands
{
    public class PatchMailingCommand : IRequest<Unit>
    {

        [JsonIgnore]
        public string? TrackingNo { get; set; }

        [JsonPropertyName("depositaryCode")]
        public string? DepositaryCode { get; set; }

        [JsonPropertyName("depositaryName")]
        public string? DepositaryName { get; set; }

        [JsonPropertyName("depositaryIdCard")]
        public string? DepositaryIdCard { get; set; }

        [JsonPropertyName("depositaryDate")]
        public DateTime? DepositaryDate { get; set; }

        [JsonPropertyName("depositaryTime")]
        public DateTime? DepositaryTime { get; set; }

        [JsonPropertyName("depositaryTr")]
        public string? DepositaryTr { get; set; }

        [JsonPropertyName("depositaryZip")]
        public string? DepositaryZip { get; set; }

        public class PatchMailingCommandHandler : IRequestHandler<PatchMailingCommand, Unit>
        {
            private IGenericRepository _repository;
            public PatchMailingCommandHandler(IGenericRepository repository)
            {
                _repository = repository;
            }

            public async Task<Unit> Handle(PatchMailingCommand request, CancellationToken cancellationToken)
            {
                await _repository.UpdateManyAsync(
                    Builders<MailingEntity>.Filter.Eq(f => f.TrackingNo, request.TrackingNo),
                    Builders<MailingEntity>.Update
                    .Set(s => s.DepositaryCode, request.DepositaryCode)
                    .Set(s => s.DepositaryName, request.DepositaryName)
                    .Set(s => s.DepositaryIdCard, request.DepositaryIdCard)
                    .Set(s => s.DepositaryDate, request.DepositaryDate)
                    .Set(s => s.DepositaryTime, request.DepositaryTime)
                    .Set(s => s.DepositaryTr, request.DepositaryTr)
                    .Set(s => s.DepositaryZip, request.DepositaryZip));

                return Unit.Value;
            }
        }
    }
}

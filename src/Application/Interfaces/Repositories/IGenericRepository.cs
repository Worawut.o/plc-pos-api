﻿using Domain.Entities;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq.Expressions;

namespace Application.Interfaces.Repositories
{
    public interface IGenericRepository
    {
        IMongoQueryable<MailingEntity> AsQueryable();

        Task<IEnumerable<MailingEntity>> FilterByAsync(
            Expression<Func<MailingEntity, bool>> filterExpression,
            Func<IMongoQueryable<MailingEntity>, IOrderedMongoQueryable<MailingEntity>>? orderBy = null,
            int? skip = null, int? take = null);

        Task<MailingEntity> FindOneAsync(Expression<Func<MailingEntity, bool>> filterExpression);
        Task UpdateManyAsync(FilterDefinition<MailingEntity> filter, UpdateDefinition<MailingEntity> update);
    }
}

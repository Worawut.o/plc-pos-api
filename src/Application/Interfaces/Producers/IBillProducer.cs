﻿using Domain.Payloads;

namespace Application.Interfaces.Producers
{
    public interface IBillProducer
    {
        Task PushClosedAsync(BillClosed contract);
    }
}

﻿using Application.Interfaces.Producers;
using Domain.Payloads;
using MediatR;

namespace Application.Events
{
    public class BillClosedEvent : INotification
    {
        public BillClosed Payload { get; set; }

        public class BillClosedEventHandler : INotificationHandler<BillClosedEvent>
        {
            protected readonly IBillProducer _billProducer;
            public BillClosedEventHandler(IBillProducer billProducer)
            {
                _billProducer = billProducer;
            }

            public async Task Handle(BillClosedEvent notification, CancellationToken cancellationToken)
            {
                await _billProducer.PushClosedAsync(notification.Payload);
            }
        }
    }
}

﻿using Application.Interfaces.Repositories;
using Domain.Entities;
using MediatR;

namespace Application.Queries
{
    public class GetMailingQuery : IRequest<MailingEntity>
    {
        public string MailingNo { get; set; }

        public class GetMailingQueryHandler : IRequestHandler<GetMailingQuery, MailingEntity>
        {
            private readonly IGenericRepository _repository;
            public GetMailingQueryHandler(IGenericRepository repository)
            {
                _repository = repository;
            }

            public async Task<MailingEntity> Handle(GetMailingQuery request, CancellationToken cancellationToken)
            {
                return await _repository.FindOneAsync(e => e.TrackingNo == request.MailingNo);
            }
        }
    }
}

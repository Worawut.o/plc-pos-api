﻿namespace Infra.Options
{
    public class MongoOption
    {
        public const string ConfigName = "ConnectionStrings";
        public string DatabaseName { get; set; }
        public string DefaultConnection { get; set; }
    }
}

﻿using Application.Interfaces.Producers;
using Domain.Payloads;
using KafkaFlow;
using KafkaFlow.Producers;

namespace Infra.Producers
{
    public class BillProducer : IBillProducer
    {
        private readonly IProducerAccessor _producerAccessor;
        public BillProducer(IProducerAccessor producerAccessor)
        {
            _producerAccessor = producerAccessor;
        }
        public async Task PushClosedAsync(BillClosed contract)
        {
            IMessageProducer messageProducer = _producerAccessor.GetProducer("posEvent") ?? throw new AggregateException("posEvent");
            await messageProducer.ProduceAsync("plc-pos-event", Guid.NewGuid().ToString(), contract);
        }
    }
}

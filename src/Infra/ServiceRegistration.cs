﻿using Application.Interfaces.Producers;
using Application.Interfaces.Repositories;
using Infra.Options;
using Infra.Producers;
using Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Infra
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceRegistration(this IServiceCollection services)
        {
            services.AddScoped<IGenericRepository, GenericRepository>();
            services.AddScoped<IBillProducer, BillProducer>();
        }

        public static void ConfigurePersistenceOptions(this IServiceCollection services)
        {
            services.AddOptions<MongoOption>().BindConfiguration(MongoOption.ConfigName);
        }
    }
}

﻿using Application.Interfaces.Repositories;
using Domain.Entities;
using Infra.Options;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq.Expressions;

namespace Infra.Repositories
{
    public class GenericRepository : IGenericRepository
    {
        private readonly MongoOption _option;
        private readonly IMongoCollection<MailingEntity> _collection;

        public GenericRepository(IOptions<MongoOption> options)
        {
            _option = options?.Value ?? throw new ArgumentNullException(nameof(options));
            var database = new MongoClient(_option.DefaultConnection).GetDatabase(_option.DatabaseName);
            _collection = database.GetCollection<MailingEntity>("tPremailing");
        }
        public IMongoQueryable<MailingEntity> AsQueryable()
        {
            return _collection.AsQueryable();
        }

        public async Task<IEnumerable<MailingEntity>> FilterByAsync(
            Expression<Func<MailingEntity, bool>> filterExpression,
            Func<IMongoQueryable<MailingEntity>, IOrderedMongoQueryable<MailingEntity>>? orderBy = null,
            int? skip = null,
            int? take = null)
        {
            var query = this.AsQueryable();

            if (filterExpression != null)
            {
                query = query.Where(filterExpression);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return await query.ToListAsync();
        }

        public Task<MailingEntity> FindOneAsync(Expression<Func<MailingEntity, bool>> filterExpression)
        {
            return Task.Run(() => _collection.Find(filterExpression).FirstOrDefaultAsync());
        }

        public Task UpdateManyAsync(FilterDefinition<MailingEntity> filter, UpdateDefinition<MailingEntity> update)
        {
            return Task.Run(delegate
            {
                _collection.UpdateManyAsync(filter, update);
            });
        }
    }
}
